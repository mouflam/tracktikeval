<?php
namespace TrackTik\Evaluation\Domain;

use TrackTik\Evaluation\Domain\Contract\Chargeable;
use TrackTik\Evaluation\Domain\Contract\Collection;
use TrackTik\Evaluation\Domain\Collection\ItemsCollection;
use TrackTik\Evaluation\Domain\Contract\ItemsCollectionInterface;
use TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException;

/**
 * Class ItemsBundle
 *
 * @package TrackTik\Evaluation\Domain
 */
class ItemsBundle implements Collection, Chargeable
{
    /**
     * @var Item
     */
    private Item $item;

    /**
     * @var ItemsCollectionInterface
     */
    private ItemsCollectionInterface $extras;

    /**
     * ItemsBundle constructor.
     *
     * @param Item                     $item
     * @param ItemsCollectionInterface $collection
     */
    private function __construct(Item $item, ?ItemsCollectionInterface $collection = null)
    {
        $this->item   = $item;
        $this->extras = $collection ?? new  ItemsCollection($item->maxExtras());
    }

    /**
     * @param Item                          $item
     * @param ItemsCollectionInterface|null $collection
     *
     * @return ItemsBundle
     */
    public static function create(Item $item, ?ItemsCollectionInterface $collection = null): ItemsBundle
    {
        return new static($item, $collection);
    }

    /**
     * @return Item
     */
    public function item(): Item
    {
        return $this->item;
    }

    /**
     * @return ItemsCollection
     */
    public function extras(): ItemsCollection
    {
        return $this->extras;
    }


    /**
     * @param Item $item
     *
     * @throws ExceededMaximumExtrasException
     */
    public function addExtraItem(Item $item): void
    {
        if ($this->extras->isMaximumExtraReached()) {
            throw new ExceededMaximumExtrasException($this->item->type(), $this->item->maxExtras());
        }

        $this->extras->add($item);
    }

    /**
     * @inheritDoc
     */
    public function price(): float
    {
        return ($this->item->price() + $this->extras->price());
    }

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return ($this->extras->count() + 1);
    }

    /**
     * @inheritDoc
     */
    public function items(string $direction = null): array
    {
        return [$this->item, ...$this->extras->items()];
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isType(string $type): bool
    {
        return ($this->item->type() === $type);
    }

    /**
     * @inheritDoc
     */
    public function hasMaximum(): bool
    {
        return false;
    }
}