<?php
namespace TrackTik\Evaluation\Domain\Collection;

use TrackTik\Evaluation\Domain\Contract\BundleCollectionInterface;
use TrackTik\Evaluation\Domain\Contract\Collection;
use TrackTik\Evaluation\Domain\Item;
use TrackTik\Evaluation\Domain\ItemsBundle;
use TrackTik\Evaluation\Domain\Exception\BundleNotFoundException;

/**
 * Class BundleCollection
 *
 * @package TrackTik\Evaluation\Domain\Collection
 */
class BundleCollection implements BundleCollectionInterface
{
    /**
     * @var ItemsBundle[]
     */
    private array $bundles = [];

    /**
     * @inheritDoc
     */
    public function add(ItemsBundle $bundle): void
    {
        $this->bundles[] = $bundle;
    }

    /**
     * @inheritDoc
     */
    public function price(): float
    {
        $price = 0;
        foreach ($this->bundles as $bundle) {
            $price += $bundle->price();
        }

        return $price;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return \count($this->bundles);
    }

    /**
     * @param string $direction
     *
     * @return Item[]
     */
    public function items(string $direction = null): array
    {
        $items = [];
        foreach ($this->bundles as $bundle) {
            foreach ($bundle->items() as $item) {
                $items[] = $item;
            }
        }
        if ($direction === Collection::ORDER_ASC) {
            usort($items, fn(Item $a, Item $b) => ($a->price() <=> $b->price()));
        }

        if ($direction === Collection::ORDER_DESC) {
            usort($items, fn(Item $a, Item $b) => ($b->price() <=> $a->price()));
        }

        return $items;
    }

    /**
     * @param string $itemType
     *
     * @return ItemsBundle
     * @throws BundleNotFoundException
     */
    public function getBundleByType(string $itemType): ItemsBundle
    {
        foreach ($this->bundles as $bundle) {
            if ($bundle->isType($itemType)) {
                return $bundle;
            }
        }

        throw new BundleNotFoundException($itemType);
    }

    /**
     * @return bool
     */
    public function hasMaximum(): bool
    {
        return false;
    }
}