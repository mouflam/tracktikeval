<?php
namespace TrackTik\Evaluation\Domain\Collection;

use TrackTik\Evaluation\Domain\Contract\ItemsCollectionInterface;
use TrackTik\Evaluation\Domain\Item;

/**
 * Class ItemsCollection
 *
 * @package TrackTik\Evaluation\Domain\Collection
 */
class ItemsCollection implements ItemsCollectionInterface
{
    /**
     * @var Item[]
     */
    private array $items = [];

    /**
     * @var int
     */
    private ?int $maxItems;

    /**
     * ItemsCollection constructor.
     *
     * @param int $maxItems
     */
    public function __construct(?int $maxItems)
    {
        $this->maxItems = $maxItems;
    }

    /**
     * @return bool
     */
    public function hasMaximum(): bool
    {
        return ($this->maxItems !== null);
    }

    /**
     * @inheritDoc
     */
    public function isMaximumExtraReached(): bool
    {
        return ($this->hasMaximum() && $this->count() >= $this->maxItems);
    }

    /**
     * @inheritDoc
     */
    public function add(Item $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @inheritDoc
     */
    public function price(): float
    {
        $price = 0;
        foreach ($this->items as $item) {
            $price += $item->price();
        }

        return $price;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return \count($this->items);
    }

    /**
     * @param string|null $direction
     *
     * @return array
     */
    public function items(string $direction = null): array
    {
        return $this->items;
    }
}