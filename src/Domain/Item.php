<?php

namespace TrackTik\Evaluation\Domain;

use TrackTik\Evaluation\Domain\Contract\Chargeable;
use TrackTik\Evaluation\Domain\Contract\ItemsCollectionInterface;
use TrackTik\Evaluation\Domain\Collection\ItemsCollection;

/**
 * Class Item
 *
 * @package TrackTik\Evaluation\Domain
 */
abstract class Item implements Chargeable
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var float
     */
    protected float $price;

    /**
     * @var int
     */
    protected ?int $maxExtras;


    /**
     * Item constructor.
     *
     * @param string   $name
     * @param float    $price
     * @param int|null $maxExtras
     */
    private function __construct(string $name, float $price, int $maxExtras = null)
    {
        $this->name      = $name;
        $this->price     = $price;
        $this->maxExtras = $maxExtras;
    }

    /**
     * @param string   $name
     * @param float    $price
     * @param int|null $maxExtras
     *
     * @return Item
     */
    public static function create(string $name, float $price, int $maxExtras = null): Item
    {
        return new static($name, $price, $maxExtras);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    static public function type(): string
    {
        return static::class;
    }

    /**
     * @return int
     */
    public function maxExtras(): ?int
    {
        return $this->maxExtras;
    }
}