<?php
namespace TrackTik\Evaluation\Domain\Contract;

use TrackTik\Evaluation\Domain\Item;

/**
 * Contract ItemsCollectionInterface
 */
interface ItemsCollectionInterface extends Collection, Chargeable
{
    /**
     * @param Item $item
     *
     * @return void
     */
    public function add(Item $item): void;

    /**
     * @return bool
     */
    public function isMaximumExtraReached(): bool ;
}