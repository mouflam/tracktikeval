<?php
namespace TrackTik\Evaluation\Domain\Contract;

/**
 * Interface Chargeable
 *
 * @package TrackTik\Evaluation\Domain\Contract
 */
interface Chargeable
{
    /**
     * @return float
     */
    public function price(): float ;
}