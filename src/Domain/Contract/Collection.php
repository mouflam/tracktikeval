<?php
namespace TrackTik\Evaluation\Domain\Contract;

/**
 * Contract Collection
 *
 * @package TrackTik\Evaluation\Domain\Collection
 */
interface Collection
{
    public const ORDER_DESC = 'desc';
    public const ORDER_ASC  = 'asc';

    /**
     * @return int
     */
    public function count(): int;

    /**
     * @param string|null $direction
     *
     * @return array
     */
    public function items(string $direction = null): array;

    /**
     * @return bool
     */
    public function hasMaximum(): bool;
}