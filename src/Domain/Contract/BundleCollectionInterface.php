<?php
namespace TrackTik\Evaluation\Domain\Contract;

use TrackTik\Evaluation\Domain\ItemsBundle;

/**
 * Contract BundleCollectionInterface
 *
 * @package TrackTik\Evaluation\Domain\Collection
 */
interface BundleCollectionInterface extends Collection, Chargeable
{
    /**
     * @param ItemsBundle $bundle
     *
     * @return void
     */
    public function add(ItemsBundle $bundle): void;
}