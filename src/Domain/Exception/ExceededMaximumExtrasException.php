<?php
namespace TrackTik\Evaluation\Domain\Exception;

/**
 * Class ExceededAllowedMaximumExtrasException
 *
 * @package TrackTik\Evaluation\Exception
 */
class ExceededMaximumExtrasException extends \Exception
{
    /**
     * ExceededAllowedMaximumExtrasException constructor.
     *
     * @param string $itemType
     * @param int    $maxExtras
     */
    public function __construct(string $itemType, int $maxExtras)
    {
        parent::__construct(sprintf('%s item allows only %s extras', $itemType, $maxExtras));
    }
}