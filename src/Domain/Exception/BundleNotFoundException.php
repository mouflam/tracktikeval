<?php
namespace TrackTik\Evaluation\Domain\Exception;

/**
 * Class BundleNotFoundException
 *
 * @package TrackTik\Evaluation\Domain\Collection
 */
class BundleNotFoundException extends \Exception
{
    /**
     * BundleNotFoundException constructor.
     *
     * @param string $bundleType
     */
    public function __construct(string $bundleType)
    {
        parent::__construct(sprintf("Bundle of type `%s` not found", $bundleType));
    }
}