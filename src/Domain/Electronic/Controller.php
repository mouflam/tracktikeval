<?php
namespace TrackTik\Evaluation\Domain\Electronic;

use TrackTik\Evaluation\Domain\Item;

/**
 * Class Television
 *
 * @package TrackTik\Evaluation\Domain\Electronic
 */
abstract class Controller extends Item
{
}