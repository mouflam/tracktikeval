<?php

namespace TrackTik\Evaluation;

use TrackTik\Evaluation\Domain\Collection\BundleCollection;
use TrackTik\Evaluation\Domain\Electronic\Console;
use TrackTik\Evaluation\Domain\Electronic\Controller\Remote;
use TrackTik\Evaluation\Domain\Electronic\Controller\Wired;
use TrackTik\Evaluation\Domain\Electronic\Microwave;
use TrackTik\Evaluation\Domain\Electronic\Television;
use TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException;
use TrackTik\Evaluation\Domain\ItemsBundle;

/**
 * Class DataBuilder
 *
 * @package TrackTik\Evaluation
 */
class DataBuilder
{
    /**
     * @return ItemsBundle
     * @throws ExceededMaximumExtrasException
     */
    public static function buildConsoleBundle(): ItemsBundle
    {
        $consoleBundle = ItemsBundle::create(Console::create('console', 12.34, 4));

        $consoleBundle->addExtraItem(Remote::create('remote', 5.7));
        $consoleBundle->addExtraItem(Remote::create('remote', 5.7));
        $consoleBundle->addExtraItem(Wired::create('wired', 6.8));
        $consoleBundle->addExtraItem(Wired::create('wired', 6.8));

        return $consoleBundle;
    }

    /**
     * @return ItemsBundle
     * @throws ExceededMaximumExtrasException
     */
    public static function buildTelevisionOneBundle(): ItemsBundle
    {
        $televisionOneBundle = ItemsBundle::create(Television::create('Television 1', 212.34));

        $televisionOneBundle->addExtraItem(Remote::create('remote', 5.7));
        $televisionOneBundle->addExtraItem(Remote::create('remote', 5.7));

        return $televisionOneBundle;
    }

    /**
     * @return ItemsBundle
     * @throws ExceededMaximumExtrasException
     */
    public static function buildTelevisionTwoBundle(): ItemsBundle
    {
        $televisionTwoBundle = ItemsBundle::create(Television::create('Television 2', 112.34));
        $televisionTwoBundle->addExtraItem(Remote::create('remote', 5.7));

        return $televisionTwoBundle;
    }

    /**
     * @return ItemsBundle
     */
    public static function buildMicrowaveBundle(): ItemsBundle
    {
        return ItemsBundle::create(Microwave::create('microwave', 32.34));
    }

    /**
     * @return BundleCollection
     * @throws ExceededMaximumExtrasException
     */
    public static function build(): BundleCollection
    {
        $bundleCollection = new BundleCollection();

        $bundleCollection->add(self::buildConsoleBundle());
        $bundleCollection->add(self::buildTelevisionOneBundle());
        $bundleCollection->add(self::buildTelevisionTwoBundle());
        $bundleCollection->add(self::buildMicrowaveBundle());

        return $bundleCollection;
    }

}



