<?php
namespace TrackTik\Evaluation\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TrackTik\Evaluation\DataBuilder;
use TrackTik\Evaluation\Domain\Collection\BundleCollection;
use TrackTik\Evaluation\Domain\Contract\Collection;
use TrackTik\Evaluation\Domain\Electronic\Console;
use TrackTik\Evaluation\Domain\Exception\BundleNotFoundException;
use TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException;

class EvaluationCommand extends Command
{
    /**
     * @return void
     */
    public function configure(): void
    {
        $this->setName('tracktik:extras')
            ->setDescription('TrackTik Test');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $outputStyle = new OutputFormatterStyle('green', 'black', ['bold', 'blink']);
        $output->getFormatter()->setStyle('fire', $outputStyle);

        $output->writeln("<fire> ____  ____    __    ___  _  _  ____  ____  _  _ ");
        $output->writeln("<fire>(_  _)(  _ \  /__\  / __)( )/ )(_  _)(_  _)( )/ )");
        $output->writeln("<fire>  )(   )   / /(__)\( (__  )  (   )(   _)(_  )  ( ");
        $output->writeln("<fire> (__) (_)\_)(__)(__)\___)(_)\_) (__) (____)(_)\_)");
        $output->writeln("");
        $output->writeln("");
        try {
            $bundleCollection = DataBuilder::build();
            $this->displayQuestion1($output, $bundleCollection);
            $this->displayQuestion2($output, $bundleCollection);

            return 0;
        } catch (ExceededMaximumExtrasException $e) {
            $output->writeln(sprintf("<fire>%s", $e->getMessage()));
        }
    }

    /**
     * @param OutputInterface $output
     * @param BundleCollection $bundleCollection
     */
    public function displayQuestion2(OutputInterface $output, BundleCollection $bundleCollection): void
    {
        try {
            $output->writeln(
                sprintf(
                    "<fire>Question 2 : The cost of console and its controllers is: $%s",
                    $bundleCollection->getBundleByType(Console::type())->price()
                )
            );
        } catch (BundleNotFoundException $e) {
            $output->writeln(sprintf("<fire>%s", $e->getMessage()));
        }
    }

    /**
     * @param OutputInterface $output
     * @param BundleCollection $bundleCollection
     */
    public function displayQuestion1(OutputInterface $output, BundleCollection $bundleCollection): void
    {
        $items = $bundleCollection->items(Collection::ORDER_DESC);
        $output->writeln(sprintf("<fire>Question 1 : List all items ordered by price of %s items", count($items)));
        $output->writeln("<fire>-----------------------------------------------------------------");
        foreach ($items as $item) {
            $output->writeln(sprintf("<fire>Item Price: $%s", $item->price()));
            $output->writeln(sprintf("<fire>Item Name: %s", $item->name()));
            $output->writeln(sprintf("<fire>Item Type: %s", $item->type()));

            $output->writeln("<fire>-----------------------------------------------------------------");
        }
        $output->writeln(sprintf("<fire>The total price is: $%s", $bundleCollection->price()));

        $output->writeln("");
        $output->writeln("");
        $output->writeln("");
    }

}