<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use TrackTik\Evaluation\DataBuilder;
use TrackTik\Evaluation\Domain\Electronic\Controller\Wired;
use TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException;
use TrackTik\Evaluation\Domain\ItemsBundle;

class ConsoleBundleTest extends TestCase
{
    /**
     * @var \TrackTik\Evaluation\Domain\ItemsBundle
     */
    private \TrackTik\Evaluation\Domain\ItemsBundle $consoleBundle;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        try {
            $this->consoleBundle = DataBuilder::buildConsoleBundle();
        } catch (ExceededMaximumExtrasException $e) {
            $this->addWarning($e->getMessage());
        }
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_on_null_price(): void
    {
        $this->assertNotNull($this->consoleBundle->price());
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_price_as_sum_of_individual_item_prices(): void
    {
        $this->assertEquals(37.34, $this->consoleBundle->price());
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_count_as_sum_of_individual_items(): void
    {
        $this->assertEquals(5, $this->consoleBundle->count());
    }

    /**
     * @test
     * @expectedException \TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException
     */
//    public function when_console_bundle_has_item_has_reached_maximum_of_its_extras_it_should_throw_exception(): void
//    {
//        $this->consoleBundle->addExtraItem(Wired::create('wired', 6.8));
//    }
}