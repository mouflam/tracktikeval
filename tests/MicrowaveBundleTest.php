<?php
namespace Tests;

use PHPUnit\Framework\TestCase;
use TrackTik\Evaluation\DataBuilder;
use TrackTik\Evaluation\Domain\Electronic\Controller\Wired;
use TrackTik\Evaluation\Domain\Exception\ExceededMaximumExtrasException;

class MicrowaveBundleTest extends TestCase
{
    /**
     * @var \TrackTik\Evaluation\Domain\ItemsBundle
     */
    private \TrackTik\Evaluation\Domain\ItemsBundle $microwaveBundle;

    protected function setUp(): void
    {
        parent::setUp();
        $this->microwaveBundle = DataBuilder::buildMicrowaveBundle();
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_on_null_price(): void
    {
        $this->assertNotNull($this->microwaveBundle->price());
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_price_as_sum_of_individual_item_prices(): void
    {
        $this->assertEquals(32.34, $this->microwaveBundle->price());
    }

    /**
     * @test
     */
    public function when_console_bundle_has_item_and_or_extra_items_should_have_count_as_sum_of_individual_items(): void
    {
        $this->assertEquals(1, $this->microwaveBundle->count());
    }

    /**
     * @test
     * @throws ExceededMaximumExtrasException
     */
    public function when_microwave_bundle_has_item_has_no_maximum_specified_of_its_extras_it_should_not_throw_exception_when_item_added(): void
    {
        $initialCount = $this->microwaveBundle->count();
        $this->microwaveBundle->addExtraItem(Wired::create('wired', 6.8));
        $this->assertEquals($initialCount + 1, $this->microwaveBundle->count());
    }
}