# Project Title

Evaluation Project
### Spec Document
You can find the test document inside the folder : 
`TrackTik_-_Back-End_Challenge.pdf`
### Prerequisites
You need to install docker software on you machine to be able to run php service.

### Time Spent
About 4hours and 30 minutes
   1. Tools Search (libraries)
   2. Modeling the problem
   3. Implementation

### Git Repository
You can download or clone the source code from:
 1. ####[Download Source Code](https://bitbucket.org/mouflam/tracktikeval.git)
 2. ####Clone Source Code
 ```
git clone https://bitbucket.org/mouflam/tracktikeval.git
```
### Installing
Follow step by step this list
```
1. git clone https://bitbucket.org/mouflam/tracktikeval.git or unzip the downloaded source file
2. cd tracktikeval or path_to_source_code
3. docker-compose up -d
4. docker-compose exec php bash
5. composer install 
    ** Not necessary : Source Code Provided With dependencies
6. php console.php tracktik:extras
```

### UML Class Diagram
![Class Diagram](./tracktik.png)

### Console Commands
```
1. php console.php

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  help             Displays help for a command
  list             Lists commands
 tracktik
  tracktik:extras  TrackTik Electronic Items
```
### Output On Console Screen With `php console.php tracktik:extras`
![Console Command Output](./output.png)

## Built With

* [php 7.4](https://www.php.net/releases/7_4_0.php) - PHP Language - Version 7.4.
* [composer](https://getcomposer.org/) - Dependency Management
* [Console Commands](https://symfony.com/doc/current/console.html) - Symfony Console Command Component

We use [Docker compose](https://docs.docker.com/compose/) To start a php server 

## Authors

* **Lamara Mouffok** <mouflam@yahoo.fr>
