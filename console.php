#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use TrackTik\Evaluation\Command\EvaluationCommand;

$application = new Application();
$application->add(new EvaluationCommand());

// ... register commands

try {
    $application->run();
} catch (Exception $e) {
    echo 'Lamara';
}